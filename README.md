# Core

**Core provides a number of general purpose code that is used throughout a number of projects for [ClifTek][ClifTek].**

[ClifTek]: http://www.cliftek.com

## Prerequisites

- **Unity 5**

## Online Resources

- [Source Code][source]
- [Documentation][documentation]

[source]: https://bitbucket.org/cliftek/core/src
[documentation]: http://www.cliftek.com/documentation/core

## Troubleshooting and support

- Bugs can be submitted via the [issue tracker][issues]

[issues]: https://bitbucket.org/cliftek/core/issues

## Quick Contributing Guide

- Fork and clone locally.
- Create a task specific branch and add your features.
- Test! Make sure the code works across platforms and within and outside the Unity Editor.
- Submit a Pull Request.

## Authors

TJ Clifton

## License

The MIT license (refer to the included LICENSE.md file)