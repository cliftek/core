//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
namespace ClifTek.Core
{
    /// <summary>
    /// This namespace holds the core classes.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc { }
}

namespace ClifTek.Core.Utility
{
    /// <summary>
    /// This namespace holds a number of utility classes in Core.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc { }
}

namespace ClifTek.Core.FileSystem
{
    /// <summary>
    /// This namespace holds a number of classes designed to help manipulate files and directories.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc { }
}
