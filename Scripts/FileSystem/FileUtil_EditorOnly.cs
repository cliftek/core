//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
#if UNITY_EDITOR
using UnityEditor;

namespace ClifTek.Core.FileSystem
{
    /// <summary>
    /// This class provides a number of utility methods to perform on a <seealso cref="File"/>.
    /// </summary>
    /// <remarks>
    /// This class is designed to provide a more transparent way of dealing with files in Unity. This
    /// can sometimes be a pain in the Unity environment as you have to constantly change between
    /// using .NET IO classes for files lying outside of the Unity project and make use of
    /// AssetDatabase for files inside the project. This class operates on <seealso cref="File"/>
    /// information to make these operations significantly simpler for client code. 
    /// </remarks>
    public static class FileUtil_EditorOnly
    {
        /// <summary>
        /// Checks whether a File with a given System Path exists on disk.
        /// </summary>
        /// <param name="zDirectorySystemPath">The file system path.</param>
        /// <returns>Whether or not the file exists.</returns>
        public static bool Exists(string zDirectorySystemPath)
        {
            return System.IO.File.Exists(zDirectorySystemPath);
        }

        /// <summary>
        /// Copies the specified source file to a destination file.
        /// </summary>
        /// <param name="zSrcFile">The source file.</param>
        /// <param name="zDstFile">The destination file.</param>
        /// <param name="zDoAssetDatabaseRefresh">if set to <c>true</c> [z do asset database refresh].</param>
        public static void Copy(File zSrcFile, File zDstFile, bool zDoAssetDatabaseRefresh = true)
        {
            //If the source and destination paths are both in the project then use asset
            //database to move and do a refresh if necessary.
            if (zSrcFile.IsInProject && zDstFile.IsInProject)
            {
                AssetDatabase.CopyAsset(zSrcFile.ProjectPath, zDstFile.ProjectPath);

                if (zDoAssetDatabaseRefresh)
                    AssetDatabase.Refresh();
            }
            //If the source path is not already in the project then use System.IO to copy the file.
            else
            {
                //Copy the system paths.
                System.IO.File.Copy(zSrcFile.SystemPath, zDstFile.SystemPath);

                //We only care about the destination path. If that's in the project then do an asset
                //refresh if requested.
                if (zDstFile.IsInProject && zDoAssetDatabaseRefresh)
                    AssetDatabase.Refresh();
            }
        }

        /// <summary>
        /// Deletes the specified file.
        /// </summary>
        /// <param name="zFile">The file to delete.</param>
        /// <param name="zDoAssetDatabaseRefresh">if set to <c>true</c> [z do asset database refresh].</param>
        public static void Delete(File zFile, bool zDoAssetDatabaseRefresh = true)
        {
            if (zFile.IsInProject)
            {
                AssetDatabase.DeleteAsset(zFile.ProjectPath);

                if (zDoAssetDatabaseRefresh)
                    AssetDatabase.Refresh();
            }
            else
                System.IO.File.Delete(zFile.SystemPath);
        }
    }
}
#endif