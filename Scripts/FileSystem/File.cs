//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;
using System;

namespace ClifTek.Core.FileSystem
{
    /// <summary>
    /// This class holds information about a file.
    /// </summary>
    /// <remarks>
    /// It can sometimes be a pain dealing with Unity and files. If a file
    /// lies outside of the project then you have to interact with it using
    /// .NET IO classes, otherwise you should use the AssetDatabase, although
    /// that doesn't cover all the operations of the .NET IO classes.
    /// 
    /// This class is designed to hold useful information about files in
    /// general. <seealso cref="ClifTek.Core.FileSystem.FileUtil_EditorOnly"/> can be used to manipulate
    /// files and will use the appropriate classes and methods to do so
    /// making files significantly easier to deal with.
    /// 
    /// <seealso cref="ClifTek.Core.FileSystem.DirectoryUtil_EditorOnly"/> also provides some file methods
    /// that are also related to directories.
    /// </remarks>
    public class File
    {
        /// <summary>
        /// Gets the full name of the file which includes the file extension.
        /// </summary>
        /// <value>
        /// The full name of the file.
        /// </value>
        public string FullFileName       { get; private set;                    }

        /// <summary>
        /// Gets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName           { get; private set;                    }

        /// <summary>
        /// Gets the extension of the file.
        /// </summary>
        /// <value>
        /// The extension.
        /// </value>
        public string Extension          { get; private set;                    }

        /// <summary>
        /// Gets the system path.
        /// </summary>
        /// <value>
        /// The system path.
        /// </value>
        public string SystemPath         { get; private set;                    }

        /// <summary>
        /// Gets the project path.
        /// </summary>
        /// <value>
        /// The project path.
        /// </value>
        public string ProjectPath        { get; private set;                    }

        /// <summary>
        /// Gets the parent directory holding this file.
        /// </summary>
        /// <value>
        /// The parent directory.
        /// </value>
        public Directory ParentDirectory { get; private set;                    }

        /// <summary>
        /// Gets a value indicating whether this file is located within the current project.
        /// </summary>
        /// <value>
        /// <c>true</c> if this file is in the project; otherwise, <c>false</c>.
        /// </value>
        public bool IsInProject          { get { return ProjectPath != "N/A"; } }

        //Private constructor to ensure File is created via static creation methods.
        private File() { }

        /// <summary>
        /// Creates a new File instance from a system path.
        /// </summary>
        /// <param name="zPath">The system path.</param>
        /// <returns>The created File instance.</returns>
        public static File CreateFromSystemPath(string zPath)
        {
            var file = new File();
            file.createFromSystemPath(zPath);
            file.initialise();
            return file;
        }

        /// <summary>
        /// Creates a new File instance from a project path.
        /// </summary>
        /// <param name="zPath">The project path.</param>
        /// <returns>The created File instance.</returns>
        public static File CreateFromProjectPath(string zPath)
        {
            var file = new File();
            file.createFromProjectPath(zPath);
            file.initialise();
            return file;
        }

        /// <summary>
        /// Checks that this File actually exists on disk.
        /// </summary>
        /// <returns>Whether or not this File exists on disk.</returns>
        public bool Exists()
        {
            return System.IO.File.Exists(SystemPath);
        }

        public override string ToString()
        {
            return "System Path = " + SystemPath + ", Project Path = " + ProjectPath;
        }

        private void createFromSystemPath(string zPath)
        {
            SystemPath = zPath;

            if (zPath.Contains(Application.dataPath))
                ProjectPath = zPath.Replace(Application.dataPath.Replace("Assets", ""), "");
            else
                ProjectPath = "N/A";
        }

        private void createFromProjectPath(string zPath)
        {
            ProjectPath = zPath;
            SystemPath = Application.dataPath.Replace("Assets", "") + ProjectPath;
        }

        private void initialise()
        {
            FullFileName = System.IO.Path.GetFileName(SystemPath);
            string[] splitFileName = FullFileName.Split(new string[] { "." }, StringSplitOptions.None);
            if (splitFileName != null && splitFileName.Length == 2)
            {
                FileName = splitFileName[0];
                Extension = splitFileName[1];
            }
            else if (splitFileName != null && splitFileName.Length == 1)
                FileName = splitFileName[0];
            else
                Log.Error("Could not intitialise file with filename: " + FullFileName);

            string[] splitFilePath = SystemPath.Split(new string[] { "\\", "/" }, StringSplitOptions.None);
            string parentDirectorySystemPath = "";
            for (int i = 0; i < splitFilePath.Length - 1; ++i)
            {
                parentDirectorySystemPath += splitFilePath[i];

                if (i < splitFilePath.Length - 2)
                    parentDirectorySystemPath += "/";
            }

            ParentDirectory = Directory.CreateFromSystemPath(parentDirectorySystemPath);
        }
    }
}
