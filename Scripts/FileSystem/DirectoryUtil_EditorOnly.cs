//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
#if UNITY_EDITOR
using UnityEditor;
using System;

namespace ClifTek.Core.FileSystem
{
    /// <summary>
    /// This class provides a number of utility methods to perform on a <seealso cref="Directory"/>.
    /// </summary>
    /// <remarks>
    /// This class is designed to provide a more transparent way of dealing with directories in Unity.
    /// This can sometimes be a pain in the Unity environment as you have to constantly change between
    /// using .NET IO classes for directories lying outside of the Unity project and make use of
    /// AssetDatabase for directories inside the project. This class operates on
    /// <seealso cref="Directory"/> and <seealso cref="File"/> information to make these operations
    /// significantly simpler for client code. 
    /// </remarks>
    public static class DirectoryUtil_EditorOnly
    {
        /// <summary>
        /// Creates a new directory on disk.
        /// </summary>
        /// <param name="zDirectory">The directory information to use to create the directory on disk.</param>
        /// <param name="zOverwriteExisting">if set to <c>true</c> [overwrite existing directory if it exists].</param>
        /// <param name="zDoAssetDatabaseRefresh">if set to <c>true</c> [do asset database refresh when complete].</param>
        public static void CreateDirectory(Directory zDirectory, bool zOverwriteExisting = false, bool zDoAssetDatabaseRefresh = true)
        {
            //If the directory is in the project then we will use the AssetDatabase methods to keep the integrity
            //of the Unity Asset Database.
            if (zDirectory.IsInProject)
            {
                //If we are overwriting an existing asset then just call delete asset. If the asset exists then it
                //will be deleted, if not this method will fail quietly.
                if (zOverwriteExisting)
                    AssetDatabase.DeleteAsset(zDirectory.ProjectPath);

                //Create a new directory if one does not already exist.
                if (!zDirectory.Exists())
                    createDirectoryInProject(zDirectory);
            }
            else
            {
                //If we are overwriting an existing directory then just call delete.
                if (zOverwriteExisting)
                    System.IO.Directory.Delete(zDirectory.SystemPath);

                //Create a new directory if one does not already exist.
                if (!zDirectory.Exists())
                    System.IO.Directory.CreateDirectory(zDirectory.SystemPath);
            }

            if (zDoAssetDatabaseRefresh)
                AssetDatabase.Refresh();
        }

        /// <summary>
        /// Retrieves an array of Directories in the given Directory.
        /// </summary>
        /// <param name="zDirectory">The directory to query.</param>
        /// <returns>The array of Directories. This will be null if no directories are found.</returns>
        public static Directory[] GetDirectories(Directory zDirectory)
        {
            //Make a system call to find all child directories - return null if none are found.
            var foundDirectories = System.IO.Directory.GetDirectories(zDirectory.SystemPath);
            if (foundDirectories == null)
                return null;

            //Iterate through found directories and fill in Directories array.
            var directories = new Directory[foundDirectories.Length];
            for (int i = 0; i < foundDirectories.Length; ++i)
            {
                //For some reason the GetDirectories method returns the last part of the path
                //with a double backslash on Windows. This will cause us problems when we try and parse
                //this string so fix up here.
                string directoryPath = foundDirectories[i].Replace("\\", "/");

                directories[i] = Directory.CreateFromSystemPath(directoryPath);
            }

            return directories;
        }

        /// <summary>
        /// Gets Files in a given Directory with a given extension.
        /// </summary>
        /// <param name="zDirectory">The Directory to query.</param>
        /// <param name="zExtension">The extension to query. This should be given with a '.', e.g. ".unity".</param>
        /// <returns>The array of Files. This will be null if no files are found.</returns>
        public static File[] GetFilesWithExtension(Directory zDirectory, string zExtension)
        {
            //Make a system call to find all child files - return null if none are found.
            var foundFiles = System.IO.Directory.GetFiles(zDirectory.SystemPath, "*" + zExtension);
            if (foundFiles == null)
                return null;

            //Iterate through found files and fill in Files array.
            var files = new File[foundFiles.Length];
            for (int i = 0; i < foundFiles.Length; ++i)
            {
                //For some reason the GetDirectories method returns the last part of the path
                //with a double backslash on Windows. This will cause us problems when we try and parse
                //this string so fix up here.
                string filePath = foundFiles[i].Replace("\\", "/");

                files[i] = File.CreateFromSystemPath(filePath);
            }

            return files;
        }

        /// <summary>
        /// Checks whether a directory with a given System Path exists on disk.
        /// </summary>
        /// <param name="zDirectorySystemPath">The directory system path.</param>
        /// <returns>Whether or not the directory exists.</returns>
        public static bool Exists(string zDirectorySystemPath)
        {
            return System.IO.Directory.Exists(zDirectorySystemPath);
        }

        private static void createDirectoryInProject(Directory zDirectory)
        {
            //To create a directory in the Unity project we are going to make a call to AssetDatabase.CreateFolder.
            //Unfortunately this will not create the directory if the parent directory does not exist so we need to
            //split the path and manually take care of this ourselves.

            //The split path holding an array of all the directories that need to exist.
            var splitPath = zDirectory.ProjectPath.Split(new string[] { "/" }, StringSplitOptions.None);

            //Make sure we could split the string ok.
            if (splitPath.Length > 1)
            {
                //Build up parent directory path
                string rootPath = "";
                for (int i = 0; i < splitPath.Length - 1; ++i)
                {
                    rootPath += splitPath[i];

                    if (i < splitPath.Length - 2)
                        rootPath += "/";
                }

                //Recursively check parent directories exist and create them first if necessary
                if (splitPath.Length > 1)
                {
                    var parentDirectory = Directory.CreateFromProjectPath(rootPath);
                    CreateDirectory(parentDirectory, false);
                }

                //Create the Directory
                AssetDatabase.CreateFolder(rootPath, splitPath[splitPath.Length - 1]);
            }
            else
                Log.Error(string.Format("Could not create a folder at '{0}' as the path is malformed.", zDirectory.ProjectPath));
        }
    }
}
#endif