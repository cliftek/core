//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;

namespace ClifTek.Core.FileSystem
{
    /// <summary>
    /// This class holds information about a directory.
    /// </summary>
    /// <remarks>
    /// It can sometimes be a pain dealing with Unity and directories. If a
    /// directory lies outside of the project then you have to interact with
    /// it using .NET IO classes, otherwise you should use the AssetDatabase,
    /// although that doesn't cover all the operations of the .NET IO classes.
    /// 
    /// This class is designed to hold useful information about directories in
    /// general. <seealso cref="DirectoryUtil_EditorOnly"/> can be used to manipulate
    /// directories and will use the appropriate classes and methods to do so
    /// making directories significantly easier to deal with.
    /// </remarks>
    public class Directory
    {
        /// <summary>
        /// Gets the name of the directory.
        /// </summary>
        /// <value>
        /// The name of the directory.
        /// </value>
        public string DirectoryName { get; private set;                    }

        /// <summary>
        /// Gets the system path.
        /// </summary>
        /// <value>
        /// The system path.
        /// </value>
        public string SystemPath    { get; private set;                    }

        /// <summary>
        /// Gets the project path.
        /// </summary>
        /// <value>
        /// The project path.
        /// </value>
        public string ProjectPath   { get; private set;                    }

        /// <summary>
        /// Gets a value indicating whether this directory is located within the current project.
        /// </summary>
        /// <value>
        /// <c>true</c> if this directory is in the project; otherwise, <c>false</c>.
        /// </value>
        public bool IsInProject     { get { return ProjectPath != "N/A"; } }

        //Private constructor to ensure Directory is created via static creation methods.
        private Directory() { }

        /// <summary>
        /// Creates a new Directory instance from a system path.
        /// </summary>
        /// <param name="zPath">The system path.</param>
        /// <returns>The created Directory instance.</returns>
        public static Directory CreateFromSystemPath(string zPath)
        {
            var directory = new Directory();
            directory.createFromSystemPath(zPath);
            directory.initialise();
            return directory;
        }

        /// <summary>
        /// Creates a new Directory instance from a project path.
        /// </summary>
        /// <param name="zPath">The project path.</param>
        /// <returns>The created Directory instance.</returns>
        public static Directory CreateFromProjectPath(string zPath)
        {
            var directory = new Directory();
            directory.createFromProjectPath(zPath);
            directory.initialise();
            return directory;
        }

        /// <summary>
        /// Checks that this Directory actually exists on disk.
        /// </summary>
        /// <returns>Whether or not this Directory exists on disk.</returns>
        public bool Exists()
        {
            return System.IO.Directory.Exists(SystemPath);
        }

        /// <summary>
        /// Determines whether this directory is a subdirectory of another.
        /// </summary>
        /// <param name="zOther">The other directory.</param>
        /// <returns>Whether or not this directory is a subdirectory of the other.</returns>
        public bool IsSubdirectoryOf(Directory zOther)
        {
            return SystemPath.StartsWith(zOther.SystemPath);
        }

        public override string ToString()
        {
            return "System Path = " + SystemPath + ", Project Path = " + ProjectPath;
        }

        private void createFromSystemPath(string zPath)
        {
            SystemPath = zPath;

            if (zPath.Contains(Application.dataPath))
                ProjectPath = zPath.Replace(Application.dataPath.Replace("Assets", ""), "");
            else
                ProjectPath = "N/A";
        }

        private void createFromProjectPath(string zPath)
        {
            ProjectPath = zPath;
            SystemPath = Application.dataPath.Replace("Assets", "") + ProjectPath;
        }

        private void initialise()
        {
            DirectoryName = System.IO.Path.GetFileName(SystemPath);
        }
    }
}
