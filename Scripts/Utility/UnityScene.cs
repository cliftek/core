﻿#if UNITY_EDITOR
//Setup a number of defines so we can call the correct API as scene management has changed a lot in Unity 5.
#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2
#define OLD_SCENE_API   //This is the old Scene API prior to Unity 5.3.
#else
#define NEW_SCENE_API   //This is when the new Scene API started to gain functionality required to be actually useful.
#endif

#if NEW_SCENE_API
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
#endif

#if OLD_SCENE_API
using ClifTek.Core;
#endif

public class UnityScene
{
    /// <summary>
    /// Gets the scene name.
    /// </summary>
    /// <value>
    /// The scene name.
    /// </value>
    public string Name { get; private set; }

    /// <summary>
    /// Gets the scene project path.
    /// </summary>
    /// <value>
    /// The scene path.
    /// </value>
    public string Path { get; private set; }

#if NEW_SCENE_API
    public UnityScene(Scene zScene)
    {
        Name = zScene.name;
        Path = zScene.path;
    }
#endif

    public UnityScene(string zName, string zPath)
    {
        Name = zName;
        Path = zPath;
    }

    /// <summary>
    /// Determines whether this scene is dirty or not.
    /// </summary>
    /// <remarks>
    /// <note type="caution">
    /// This method will always return true in versions of Unity before 5.3. This
    /// is because there was no reliable way of detecting this.
    /// </note>
    /// </remarks>
    /// <returns>true if the scene is dirty, false otherwise.</returns>
    public bool IsDirty()
    {
#if NEW_SCENE_API
        var scene = EditorSceneManager.GetSceneByPath(Path);
        return scene.isDirty;
#else
        Log.Info("Cannot detect dirty scenes prior to Unity 5.3. Assuming dirty.");
        return true;
#endif
    }

#if NEW_SCENE_API
    /// <summary>
    /// Gets Unity's Scene structure for this scene
    /// </summary>
    /// <remarks>
    /// <note type="caution">
    /// The method that retrieves the Scene struct will only work if this scene
    /// is currently pen in Unity. This is a limitation of the EditorSceneManager API.
    /// </note>
    /// </remarks>
    /// <returns>The scene</returns>
    public Scene GetScene()
    {
        return EditorSceneManager.GetSceneByPath(Path);
    }
#endif
}
#endif