//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace ClifTek.Core.Utility
{
    /// <summary>
    /// This class replaces the default Unity Transform Editor as it is quite basic.
    /// </summary>
    /// <seealso cref="UnityEditor.Editor" />
    [CustomEditor(typeof(Transform)), CanEditMultipleObjects]
    class Transform_Editor : Editor
    {
        // - Private -----------------------------------------------------
        private Vector3 m_eulerAngles;
        private Quaternion m_prevRotationQuaternion = Quaternion.identity;
        private SerializedProperty m_position;
        private SerializedProperty m_rotation;
        private SerializedProperty m_scale;

        private MethodInfo m_tempContentInfo;
        private MethodInfo m_sendScaleMethodInfo;

        public override void OnInspectorGUI()
        {
            Transform transform = (Transform)target;
            serializedObject.Update();
            inspector3D(transform);

            EditorGUI.showMixedValue = false;
            serializedObject.ApplyModifiedProperties();
        }

        #region Unity Messages
        private void OnEnable()
        {
            m_tempContentInfo = typeof(EditorGUIUtility).GetMethod("TextContent", BindingFlags.NonPublic | BindingFlags.Static);
            m_sendScaleMethodInfo = typeof(Transform).GetMethod("SendTransformChangedScale", BindingFlags.NonPublic | BindingFlags.Instance);

            m_position = serializedObject.FindProperty("m_LocalPosition");
            m_scale = serializedObject.FindProperty("m_LocalScale");
        }
        #endregion

        private void inspector3D(Transform zTransform)
        {
            GUILayout.Space(3f);

            // - Position -
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Position", GUILayout.Width(60f)))
            {
                updateUndoRecord();
                m_position.vector3Value = Vector3.zero;
            }
            EditorGUILayout.PropertyField(m_position, (GUIContent)m_tempContentInfo.Invoke(null, new object[] { "" }), new GUILayoutOption[0]);
            EditorGUILayout.EndHorizontal();

            // - Rotation -
            Quaternion localRotation = zTransform.localRotation;
            if (m_prevRotationQuaternion.x != localRotation.x || m_prevRotationQuaternion.y != localRotation.y || m_prevRotationQuaternion.z != localRotation.z || m_prevRotationQuaternion.w != localRotation.w)
            {
                m_eulerAngles = zTransform.localEulerAngles;
                m_prevRotationQuaternion = localRotation;
            }

            bool showMixedValueFlag = false;
            Object[] targets = base.targets;
            for (int i = 0; i < targets.Length; i++)
            {
                Transform transform2 = (Transform)targets[i];
                showMixedValueFlag |= (transform2.localEulerAngles != m_eulerAngles);
            }

            EditorGUILayout.BeginHorizontal();
            bool hasRotationChanged = false;
            if (GUILayout.Button("Rotation", GUILayout.Width(60f)))
            {
                m_eulerAngles = Vector3.zero;
                hasRotationChanged = true;
            }
            EditorGUI.showMixedValue = showMixedValueFlag;
            EditorGUI.BeginChangeCheck();
            m_eulerAngles = EditorGUILayout.Vector3Field("", m_eulerAngles, new GUILayoutOption[0]);
            if (EditorGUI.EndChangeCheck() || hasRotationChanged)
            {
                updateUndoRecord();
                updateRotation(localRotation);
            }
            EditorGUILayout.EndHorizontal();

            // - Scale -
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Scale", GUILayout.Width(60f)))
            {
                m_scale.vector3Value = Vector3.one;
            }
            EditorGUILayout.PropertyField(m_scale, (GUIContent)m_tempContentInfo.Invoke(null, new object[] { "" }), new GUILayoutOption[0]);
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(1f);

            EditorGUILayout.Space();

            // - Reset All -
            if (GUILayout.Button("Reset All"))
            {
                updateUndoRecord();
                m_position.vector3Value = Vector3.zero;

                m_scale.vector3Value = Vector3.one;
            }
        }

        private void updateRotation(Quaternion zPrevRotation)
        {
            Object[] targets2 = base.targets;
            for (int j = 0; j < targets2.Length; j++)
            {
                Transform transform3 = (Transform)targets2[j];
                transform3.localEulerAngles = m_eulerAngles;
                if (transform3.parent != null)
                    m_sendScaleMethodInfo.Invoke(transform3, null);
            }

            serializedObject.SetIsDifferentCacheDirty();
            m_prevRotationQuaternion = zPrevRotation;
        }

        private void updateUndoRecord()
        {
            Undo.RecordObjects(base.targets, "Transform Change");
        }
    }
}
