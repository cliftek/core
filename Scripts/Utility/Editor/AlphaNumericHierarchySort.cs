//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;
using UnityEditor;

namespace ClifTek.Core.Utility
{
    /// <summary>
    /// Arranges the Unity Hierarchy view in the old AlphaNumeric order.
    /// </summary>
    public class AlphaNumericHierarchySort : BaseHierarchySort
    {
        public override int Compare(GameObject lhs, GameObject rhs)
        {
            if (lhs == rhs)
                return 0;
            if (lhs == null)
                return -1;
            if (rhs == null)
                return 1;

            return EditorUtility.NaturalCompare(lhs.name, rhs.name);
        }
    }
}
