//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
#if UNITY_EDITOR
//Setup a number of defines so we can call the correct API as scene management has changed a lot in Unity 5.
#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2
#define OLD_SCENE_API   //This is the old Scene API prior to Unity 5.3.
#elif UNITY_5_3_0 || UNITY_5_3_1
#define PARTIAL_NEW_SCENE_API   //This was when the new Scene API was introduced, although it was lacking in a fair amount of functionality.
#else
#define NEW_SCENE_API   //This is when the new Scene API started to gain functionality required to be actually useful.
#endif
using UnityEngine;
using UnityEditor;
#if !OLD_SCENE_API
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
#else
using ClifTek.Core;
#endif
using System.Collections.Generic;

namespace ClifTek.Core.Utility
{
    /// <summary>
    /// This class provides a number of utility methods for dealing with objects and scenes.
    /// </summary>
    public static class SceneUtils_EditorOnly
    {
        /// <summary>
        /// Resets all world components of the game object transform.
        /// </summary>
        /// <param name="zGameObject">The game object.</param>
        public static void ResetGameObjectTransform(GameObject zGameObject)
        {
            zGameObject.transform.position = Vector3.zero;
            zGameObject.transform.rotation = Quaternion.identity;
            zGameObject.transform.localScale = Vector3.one;
        }

        /// <summary>
        /// Resets all local components game object local transform.
        /// </summary>
        /// <param name="zGameObject">The game object.</param>
        public static void ResetGameObjectLocalTransform(GameObject zGameObject)
        {
            zGameObject.transform.localPosition = Vector3.zero;
            zGameObject.transform.localRotation = Quaternion.identity;
            zGameObject.transform.localScale = Vector3.one;
        }

        /// <summary>
        /// Gets all root game objects in all loaded scenes.
        /// </summary>
        /// <param name="zInAllScenes">if set to <c>true</c> [return root game objects in all loaded scenes]. 
        /// if set to >c?false> [return root game objects in active scene only].</param>
        /// <returns>All root game objects.</returns>
        public static List<GameObject> GetRootGameObjects(bool zInAllScenes = true)
		{
#if NEW_SCENE_API
            List<GameObject> rootGOs = new List<GameObject>();
            if (zInAllScenes)
            {
                for (int i = 0; i < EditorSceneManager.sceneCount; ++i)
                    rootGOs.AddRange(GetRootGameObjects(EditorSceneManager.GetSceneAt(i)));
            }
            else
                rootGOs.AddRange(GetRootGameObjects(EditorSceneManager.GetActiveScene()));

            return rootGOs;
#elif PARTIAL_NEW_SCENE_API
            if (zInAllScenes)
                 return new List<GameObject>(getRootGameObjects());
            else
                return GetRootGameObjects(EditorSceneManager.GetActiveScene());
#else
            //In old API there was no concept of multiple scenes so just return all root objects.
            return new List<GameObject>(getRootGameObjects());
#endif
		}

        /// <summary>
        /// Gets all root game objects in a particular scene.
        /// </summary>
        /// <param name="zScene">The scene to look for root game objects in.</param>
        /// <returns>All root game objects in the given scene.</returns>
        public static List<GameObject> GetRootGameObjects(UnityScene zScene)
        {
#if OLD_SCENE_API
            //In old API there was no concept of multiple scenes so just return all root objects.
            return new List<GameObject>(getRootGameObjects());
#else
            return GetRootGameObjects(zScene.GetScene());
#endif
        }

#if !OLD_SCENE_API
        /// <summary>
        /// Gets all root game objects in a particular scene.
        /// </summary>
        /// <param name="zScene">The scene to look for root game objects in.</param>
        /// <returns>All root game objects in the given scene.</returns>
        public static List<GameObject> GetRootGameObjects(Scene zScene)
        {
#if PARTIAL_NEW_SCENE_API
            //Get all root game objects in all scenes.
            var allRootGameObjects = GetRootGameObjects(true);
            var rootGameObjects = new List<GameObject>();

            //Find all root objects that belong to the scene we're interested in.
            foreach (var rootGO in allRootGameObjects)
            {
                if (rootGO.scene.path == zScene.path)
                    rootGameObjects.Add(rootGO);
            }

            return rootGameObjects;
#elif NEW_SCENE_API
            return new List<GameObject>(zScene.GetRootGameObjects());
#endif
        }
#endif

        /// <summary>
        /// Determines whether the active scene is dirty.
        /// </summary>
        /// <returns>Whether or not the active scene is dirty.</returns>
        public static bool IsActiveSceneDirty()
        {
#if OLD_SCENE_API
            Log.Info("Cannot detect dirty scenes prior to Unity 5.3. Assuming dirty.");
            return true;
#else
            return EditorSceneManager.GetActiveScene().isDirty;
#endif
        }

        /// <summary>
        /// Determines whether any active scenes are dirty.
        /// </summary>
        /// <returns>Whether or not any active scenes are dirty.</returns>
        public static bool AreAnyActiveScenesDirty()
        {
#if OLD_SCENE_API
            Log.Info("Cannot detect dirty scenes prior to Unity 5.3. Assuming dirty.");
            return true;
#else
            bool isDirty = false;
            for (int i = 0; i < EditorSceneManager.sceneCount; ++i)
                isDirty |= EditorSceneManager.GetSceneAt(i).isDirty;

            return isDirty;
#endif
        }

        /// <summary>
        /// Determines whether a scene with a given path is dirty.
        /// </summary>
        /// <param name="zScenePath">The path for the scene to check..</param>
        /// <returns>Whether or not the scene is dirty.</returns>
        public static bool IsSceneDirty(string zScenePath)
        {
#if OLD_SCENE_API
            Log.Info("Cannot detect dirty scenes prior to Unity 5.3. Assuming dirty.");
            return true;
#else
            var scene = EditorSceneManager.GetSceneByPath(zScenePath);
            return scene.isDirty;
#endif
        }

        private static IEnumerable<GameObject> getRootGameObjectsWithOldSceneAPI()
        {
            //This is a bit of a hack to get the root game objects in a scene
			//in Unity prior to multiple scenes being supported. After that
			//the scenes themselves get returned, but not as GameObjects, which
			//results in an array of null GameObjects with a length equal to
			//the number of loaded scenes.
			var property = new HierarchyProperty(HierarchyType.GameObjects);
			var expanded = new int[0];
			while (property.Next(expanded))
				yield return property.pptrValue as GameObject;
        }

#if OLD_SCENE_API
		private static IEnumerable<GameObject> getRootGameObjects()
		{
			foreach (var rootGO in getRootGameObjectsWithOldSceneAPI())
				yield return rootGO;
		}
#endif

#if PARTIAL_NEW_SCENE_API
        private static IEnumerable<GameObject> getRootGameObjects()
        {
            //Unity 5.3.0 introduced the new Scene API that allows you to manipulate scenes
            //using the SceneManagement classes. Unfortunately the method to GetRootGameObjects
            //was only added to the Scene class in Unity 5.3.3 and the hack that used to work
            //prior to the new Scene API will now just return null Game Objects when used when
            //more than 1 scene is loaded. It does however still work when just 1 scene is loaded.

            //Check how many scenes we have loaded.
            int numberOfLoadedScenes = EditorSceneManager.GetAllScenes().Length;
            Log.Info("Number of loaded scenes: " + numberOfLoadedScenes);

            //If we just have the 1 scene loaded then we can use the old hack ok.
            if (numberOfLoadedScenes == 1)
            {
                foreach (var rootGO in getRootGameObjectsWithOldSceneAPI())
                    yield return rootGO;
            }
            //If we have multiple scenes loaded then we are going to resort to a bit of a nasty
            //solution and grab all Transforms in all scenes and traverse the list to find all
            //root objects (those with the parent transform set to null) - This is going to be SLOW!
            else
            {
                var allTransforms = GameObject.FindObjectsOfType<Transform>();

                foreach (Transform t in allTransforms)
                {
                    if (t != null && t.parent == null)
                        yield return t.gameObject;
                }
            }
        }
#endif

        
        [MenuItem("GameObject/Show all game objects")]
        private static void showAllGameObjsctsInLevel()
        {
            var allGameObjects = findAllGameObjects();
            showAll(allGameObjects);
        }

        private static void showAll(GameObject[] zGameObjects)
        {
            foreach (var go in zGameObjects)
            {
                if (go.hideFlags != 0)
                {
                    go.hideFlags = 0;
                    EditorUtility.SetDirty(go);
                }
            }
        }

        private static GameObject[] findAllGameObjects()
        {
            //Resorces.FindObjectsOfTypeAll will return all matching game objects, including
            //inactive game objects which GameObject.FindGameObbjectsOfType will not.
            var allGameObjects = Resources.FindObjectsOfTypeAll<GameObject>();
            var gameObjects = new List<GameObject>();
            foreach (var go in allGameObjects)
            {
                //IF this is a root game object with one of the names below then these are important editor
                //objects - mucking around with their hide flags or modifying/deleting them can cause BAD
                //things to happen so don't even return these objects.
                if (go.transform.parent == null)
                {
                    if (go.name == "SceneLight" || go.name == "SceneCamera")
                        continue;
                }

                gameObjects.Add(go);
            }

            return gameObjects.ToArray();
        }
    }
}
#endif