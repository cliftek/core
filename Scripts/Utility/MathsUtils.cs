//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;

namespace ClifTek.Core.Utility
{
    /// <summary>
    /// Various maths based utility methods.
    /// </summary>
    public static class MathsUtils
    {
        /// <summary>
        /// The square epsilon will be of use when we want to compare squared quantities for performance
        /// reasons rather than using a square root
        /// </summary>
        public static float SqrEpsilon { get { return float.Epsilon * float.Epsilon; } }

        /// <summary>
        /// The epsilon will be of use when we want to compare quantities
        /// </summary>
        public static float Epsilon { get { return float.Epsilon; } }

        /// <summary>
        /// This method wraps a number between a minimum and maximum limit
        /// </summary>
        /// <param name="zInput">The number to wrap</param>
        /// <param name="zFrom">The lower limit</param>
        /// <param name="zTo">The upper limit</param>
        /// <returns>The wrapped number</returns>
        public static float Wrap(float zInput, float zFrom, float zTo)
        {
            float wrappedValue = zInput;

            //If the number is less than the limit then wrap it round to the upper limit
            //minus the amount it is below the limit by
            if (zInput < zFrom)
                wrappedValue = zTo - (zFrom - zInput);
            //If the number is higher than the limit then wrap it round to the lower limit
            //plus the amount it is above the limit by
            else if (zInput > zTo)
                wrappedValue = zFrom + (zInput - zTo);

            return wrappedValue;
        }

        /// <summary>
        /// Wrap a Vector3 between a minimum and maximum limit
        /// </summary>
        /// <param name="zInput">The vector to wrap</param>
        /// <param name="zFrom">The lower limit</param>
        /// <param name="zTo">The upper limit</param>
        /// <returns>The wrapped vector</returns>
        public static Vector3 Wrap(Vector3 zInput, Vector3 zFrom, Vector3 zTo)
        {
            Vector3 wrappedValue;

            //Wraps each of the vector quantities using the float wrap method
            wrappedValue.x = Wrap(zInput.x, zFrom.x, zTo.x);
            wrappedValue.y = Wrap(zInput.y, zFrom.y, zTo.y);
            wrappedValue.z = Wrap(zInput.z, zFrom.z, zTo.z);

            return wrappedValue;
        }

        /// <summary>
        /// Generates a random number between 0 and 1 with values around 0 more likely
        /// </summary>
        /// <returns>The random binomial</returns>
        public static float RandomBinomial()
        {
            return Random.Range(0f, 1f) - Random.Range(0f, 1f);
        }

        /// <summary>
        /// Calculates the squared distance between two vectors.
        /// </summary>
        /// <param name="zVec1">The first vector to use</param>
        /// <param name="zVec2">The second vector to use</param>
        /// <returns>The squared distance between the two vectors</returns>
        /// <remarks>
        /// This method is useful for collision detection where you just want to compare distances. You can
        /// instead compare two squared distances which saves a costly square root calculation per distance,
        /// </remarks>
        public static float SquaredDistance(Vector3 zVec1, Vector3 zVec2)
        {
            return Mathf.Pow(zVec2.x - zVec1.x, 2) + Mathf.Pow(zVec2.y - zVec1.y, 2) + Mathf.Pow(zVec2.z - zVec1.z, 2);
        }
    }
}
