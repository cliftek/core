//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;
using System;

namespace ClifTek.Core.Utility
{
    /// <summary>
    /// A utility method to help with setting GUI Colours.
    /// </summary>
    /// <seealso cref="IDisposable" />
    /// <seealso href="http://www.tjclifton.com/unity-gui-colour-utility/"/>
    public class GUIColour : IDisposable
    {
        /// <summary>
        /// The type of GUI Colour to set.
        /// </summary>
        public enum GUIColourType
        {
            /// <summary>
            /// GUI.color
            /// </summary>
            Color,
            /// <summary>
            /// GUI.backgroundColor
            /// </summary>
            BackgroundColor,
            /// <summary>
            /// GUI.contentColor
            /// </summary>
            ContentColor,
            /// <summary>
            /// Gizmos.color
            /// </summary>
            Gizmos
        }

        private readonly Color k_cachedColour;
        private readonly GUIColourType k_colourType;

        /// <summary>
        /// Initializes a new instance of the <see cref="GUIColour"/> class.
        /// </summary>
        /// <param name="zColour">The colour to use.</param>
        /// <param name="zColourType">Type of GUIcolour.</param>
        public GUIColour(Color zColour, GUIColourType zColourType = GUIColourType.Color)
        {
            k_cachedColour = getCurrentColour(zColourType);
            k_colourType = zColourType;

            setCurrentColour(zColour);
        }

        /// <summary>
        /// Restores cached GUI Colour.
        /// </summary>
        public void Dispose()
        {
            setCurrentColour(k_cachedColour);
        }

        private Color getCurrentColour(GUIColourType zColourType)
        {
            switch (zColourType)
            {
                case GUIColourType.Color: return GUI.color;
                case GUIColourType.BackgroundColor: return GUI.backgroundColor;
                case GUIColourType.ContentColor: return GUI.contentColor;
                case GUIColourType.Gizmos: return Gizmos.color;

                default: return Colours.White;
            }
        }

        private void setCurrentColour(Color zColour)
        {
            switch (k_colourType)
            {
                case GUIColourType.Color: GUI.color = zColour; break;
                case GUIColourType.BackgroundColor: GUI.backgroundColor = zColour; break;
                case GUIColourType.ContentColor: GUI.contentColor = zColour; break;
                case GUIColourType.Gizmos: Gizmos.color = zColour; break;
                default: Log.Warning("Could not set colour for type: " + k_colourType); break;
            }
        }
    }
}
