//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace ClifTek.Core.Utility
{
    /// <summary>
    /// A number of utility methods for use in creating Editor GUI.
    /// </summary>
    public static class EditorUtils_EditorOnly
    {
        /// <summary>
        /// Begins the contents.
        /// </summary>
        /// <remarks>
        /// Should be followed by <see cref="EndContents"/>.
        /// </remarks>
        /// <param name="zStr">The string for BeginHorizontal.</param>
        public static void BeginContents(string zStr = "AS TextArea")
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(4f);
            EditorGUILayout.BeginHorizontal(zStr, GUILayout.MinHeight(10f));
            GUILayout.BeginVertical();
            GUILayout.Space(2f);
        }

        /// <summary>
        /// Ends the contents.
        /// </summary>
        /// <remarks>
        /// Should be preceded by <see cref="BeginContents(string)"/>
        /// </remarks>
        public static void EndContents()
        {
            GUILayout.Space(3f);
            GUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(3f);
            GUILayout.EndHorizontal();
            GUILayout.Space(3f);
        }

        /// <summary>
        /// Converts a text and tool tip string to GUIContent.
        /// </summary>
        /// <param name="zTextAndTooltip">The text and tooltip.</param>
        /// <returns>The resulting GUIContent.</returns>
        public static GUIContent TextContent(string zTextAndTooltip)
        {
            if (zTextAndTooltip == null)
                zTextAndTooltip = string.Empty;

            string[] nameAndToolTip = GetNameAndToolTipStrings(zTextAndTooltip);
            var guiContent = new GUIContent(nameAndToolTip[0]);
            if (nameAndToolTip[1] != null)
                guiContent.tooltip = nameAndToolTip[1];

            return guiContent;
        }

        /// <summary>
        /// Gets the name and tool tip strings.
        /// </summary>
        /// <param name="zNameAndToolTip">The name and tool tip.</param>
        /// <returns>An array of length 2 with name at index 0 and tooltip at index 1</returns>
        public static string[] GetNameAndToolTipStrings(string zNameAndToolTip)
        {
            string[] result = new string[2];
            string[] splitNameAndToolTip = zNameAndToolTip.Split(new string[] { "|" }, System.StringSplitOptions.None);

            switch (splitNameAndToolTip.Length)
            {
                case 0:
                    result[0] = result[1] = string.Empty; break;
                case 1:
                    result[0] = splitNameAndToolTip[0].Trim();
                    result[1] = string.Empty;
                    break;
                case 2:
                    result[0] = splitNameAndToolTip[0].Trim();
                    result[1] = splitNameAndToolTip[1].Trim();
                    break;
                default:
                    Log.Error("Could not get name and ToolTip strings from: " + zNameAndToolTip);
                    break;
            }

            return result;
        }
    }
}
#endif
