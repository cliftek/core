//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;
using System;

namespace ClifTek.Core.Utility
{
    /// <summary>
    /// Various general purpose utility methods.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Get a component in a parent Transform.
        /// </summary>
        /// <remarks>
        /// A helper method that recursively calls GetComponent on all parents of an initial transform
        /// until a component is found or until there are no parents left to check. This method is slow.
        /// As of Unity 4.5 there is a GetComponentInParent() method that does the same thing so that should
        /// be used on that version of Unity and up.
        /// </remarks>
        /// <typeparam name="T">The type of component to find</typeparam>
        /// <param name="zTransform">The base transform</param>
        /// <returns>The component or null if no component was found</returns>
        public static T GetComponentInParent<T>(Transform zTransform) where T : Component
        {
            //Get parent of transform
            var parent = zTransform.parent;

            //If parent is null then we are at the root of the hierarchy so there is no component in any
            //parent of the initial transform
            if (parent == null)
                return default(T);

            var component = parent.GetComponent<T>();

            //If we have successfully found the component then return it
            if (component != null)
                return component;

            //otherwise recursively call the same method using the parent as the new base transform
            return GetComponentInParent<T>(parent);
        }

        /// <summary>
        /// A helper method to get a unique version of an asset name.
        /// </summary>
        /// <remarks>
        /// This method is useful when you want to create assets of a basic name, which you want to
        /// ensure are unique. This method will return the given basic name with a suffix if required
        /// to ensure it is unique. So for example if you wanted to create a new scene and default to
        /// calling it "New Scene" but there was already a scene with that name this method would return
        /// "New Scene_1".
        /// </remarks>
        /// <param name="zDefaultName">The default name..</param>
        /// <param name="zPredicate">The predicate that determines whether or not the proposed name is ok.</param>
        /// <returns>The new name,</returns>
        public static string GetNewAssetName(string zDefaultName, Func<string, bool> zPredicate)
        {
            //Setup
            string pendingName = zDefaultName;
            int suffix = 0;

            //Keep going until we have an acceptable name
            while (true)
            {
                //Use predicate to find out if this name is ok
                bool isOK = zPredicate(pendingName);

                //If this name is ok then break out of loop
                if (isOK)
                    break;

                //If not then update the pending name and try again
                ++suffix;
                pendingName = zDefaultName + "_" + suffix.ToString();
            }

            return pendingName;
        }
    }
}
