//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;
using System;
using System.Collections.Generic;

namespace ClifTek.Core
{
    /// <summary>
    /// Contains the actual database to be used by the Custom Database
    /// </summary>
    [Serializable]
    public class CustomDatabaseData
    {
        //These lists need to be kept in sync.
        [SerializeField] private List<string> m_keys = new List<string>();
        [SerializeField] private List<string> m_values = new List<string>();

        /// <summary>
        /// Adds an entry.
        /// </summary>
        /// <param name="zKey">The key.</param>
        /// <param name="zValue">The value.</param>
        /// <returns>Whether or not the entry was added successfully.</returns>
        public bool AddEntry(string zKey, string zValue)
        {
            if (!m_keys.Contains(zKey))
            {
                m_keys.Add(zKey);
                m_values.Add(zValue);
            }
            else
            {
                int keyIndex = getKeyIndex(zKey);
                m_values[keyIndex] = zValue;
            }

            return true;
        }

        /// <summary>
        /// Removes an entry.
        /// </summary>
        /// <param name="zKey">The key.</param>
        /// <returns>Whether or not the entry was removed successfully.</returns>
        public bool RemoveEntry(string zKey)
        {
            int keyIndex = getKeyIndex(zKey);
            if (keyIndex != -1)
            {
                m_keys.RemoveAt(keyIndex);
                m_values.RemoveAt(keyIndex);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets an entry.
        /// </summary>
        /// <param name="zKey">The key.</param>
        /// <returns>The value if the key exists, otherwise an empty string.</returns>
        public string GetEntry(string zKey)
        {
            int keyIndex = getKeyIndex(zKey);
            if (keyIndex != -1)
                return m_values[keyIndex];
            else
            {
                Log.Info("Could not find value for entry with key = " + zKey);
                return string.Empty;
            }
        }

        private int getKeyIndex(string zKey)
        {
            return m_keys.FindIndex(x => x == zKey);
        }
    }
}
