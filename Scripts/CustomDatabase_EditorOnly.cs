//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
#if UNITY_EDITOR
using UnityEditor;
using ClifTek.Core.FileSystem;

namespace ClifTek.Core
{
    public partial class CustomDatabase
    {
        /// <summary>
        /// Creates the Custom Database on disk.
        /// </summary>
        /// <param name="zDatabase">The database to create the asset from.</param>
        public static void Create_EditorOnly(CustomDatabase zDatabase)
        {
            //Make sure directory exists
            if (!zDatabase.DatabaseFile.Exists())
                DirectoryUtil_EditorOnly.CreateDirectory(zDatabase.DatabaseFile.ParentDirectory, zDoAssetDatabaseRefresh: false);

            AssetDatabase.CreateAsset(zDatabase, zDatabase.DatabaseFile.ProjectPath);
        }

        /// <summary>
        /// Saves the whole Custom Database.
        /// </summary>
        /// <param name="zDoRefresh">if set to <c>true</c> [do AssetDatabase refresh].</param>
        public void Save_EditorOnly(bool zDoRefresh = true)
        {
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();

            if (zDoRefresh)
                AssetDatabase.Refresh();
        }

        /// <summary>
        /// Adds an entry to the database.
        /// </summary>
        /// <param name="zKey">The key to add.</param>
        /// <param name="zValue">The value to add.</param>
        /// <returns>Whether or not the entry was added successfully.</returns>
        public bool Add_EditorOnly(string zKey, string zValue)
        {
            return m_data.AddEntry(zKey, zValue);
        }

        /// <summary>
        /// Removes an entry from the database.
        /// </summary>
        /// <param name="zKey">The key.</param>
        /// <returns>Whether or not the entry was removed successfully.</returns>
        public bool Remove_EditorOnly(string zKey)
        {
            return m_data.RemoveEntry(zKey);
        }
    }
}
#endif
