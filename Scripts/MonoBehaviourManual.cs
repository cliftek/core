//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;

namespace ClifTek.Core
{
    /// <summary>
    /// This class is designed to have its methods called by the equivalent MonoBehvaiour methods.
    /// </summary>
    /// <remarks>
    /// This class can be placed on GameObjects like a normal MonoBehaviour but gives us more
    /// control over the order of execution of standard methods by specifying manual versions.
    /// 
    /// These classes will typically be called by some controlling class.
    /// 
    /// Use this over the MonoBehaviourManual where you need a Component but need more control
    /// over execution order of code.
    /// </remarks>
    public class MonoBehaviourManual : MonoBehaviour
    {
        /// <summary>
        /// Custom Awake method to be called in code at Awake time
        /// </summary>
        public virtual void AwakeManual()                     { }

        /// <summary>
        /// Custom Start method to be called in code at Start time
        /// </summary>
        public virtual void StartManual()                     { }

        /// <summary>
        /// Custom Update method to be called in code during Update time
        /// </summary>
        public virtual void UpdateManual(float zDT)           { }

        /// <summary>
        /// Custom LateUpdate method to be called in code at LateUpdate time
        /// </summary>
        public virtual void LateUpdateManual()                { }

        /// <summary>
        /// Custom FixedUpdate method to be called in code during FixedUpdate time
        /// </summary>
        public virtual void FixedUpdateManual(float zFixedDT) { }
    }
}
