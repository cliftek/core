//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
#if !UNITY_EDITOR
//TC: This define should be set if you want to strip the log out of a build. It is recommended
//to set this as a scripting symbol at build time rather than defining it manually here.
//#define STRIP_DEBUG_LOG
using UnityEngine;
using System.Text;
using System.Collections.Generic;

namespace ClifTek.Core
{
    /// <summary>
    /// This class is a facade to act as a wrapper around Unity's default Logger.
    /// </summary>
    /// <remarks>
    /// This class is designed to address some issues with the default Unity Logger.
    /// 
    /// It enables logging to be stripped out completely by defining STRIP_DEBUG_LOG,
    /// which can provide a performance boost for release build without having to track
    /// down the log code and remove it manually.
    /// 
    /// This class is only used outside of the Unity editor. Within the Editor itself a
    /// class in the provided Logger.dll provides the same functionality. The reason for
    /// this is that overwriting the default Logger means that when you double-click on
    /// a log message in the Unity Console you get taken to this class which prints out
    /// the message which is not useful and incredibly frustrating! Placing another class
    /// in a dll for the Editor means that Unity can't trace the callstack all the way and
    /// instead displays the last method call it can find, which will be the line of code
    /// that called this class. (which is what we want!)
    /// 
    /// Additionally this class also allows a user to specify a filter which can be turned
    /// on and off at runtime and makes the default Unity Console a little easier to deal
    /// with as it cuts out some noise that can accumulate over time in a big project.
    /// </remarks>
    public static class Log
    {
        // - Public ---------------------------------------------------------------------------------
        // - Constants -
        public const string k_GeneralFilter = "GENERAL";

        // - Private --------------------------------------------------------------------------------
        private static bool s_isIntitialised = false;
        private static Dictionary<string, bool> s_filtersDictionary = new Dictionary<string, bool>();

        /// <summary>
        /// Log a standard information message.
        /// </summary>
        /// <param name="zMessage">The message to log.</param>
        /// <param name="zFilter">The filter to use. Defaults to the General filter.</param>
        /// <param name="zContext">An optional context to pass to the Unity Logger.</param>
#if STRIP_DEBUG_LOG
	    [System.Diagnostics.Conditional("FALSE")]
#endif
	    public static void Info(object zMessage, string zFilter = k_GeneralFilter, Object zContext = null)
	    {
		    initialiseIfNecessary();
	
		    //Only log if filter is set and enabled
		    if (s_filtersDictionary.ContainsKey(zFilter) && s_filtersDictionary[zFilter])
		    {
			    var logOutput = getFilterString(zFilter, zMessage);
			    if (zContext == null)
				    Debug.Log(logOutput);
			    else
				    Debug.Log(logOutput, zContext);
		    }
	    }

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="zMessage">The message to log.</param>
        /// <param name="zFilter">The filter to use. Defaults to the General filter.</param>
        /// <param name="zContext">An optional context to pass to the Unity Logger.</param>
#if STRIP_DEBUG_LOG
	    [System.Diagnostics.Conditional("FALSE")]
#endif
	    public static void Warning(object zMessage, string zFilter = k_GeneralFilter, Object zContext = null)
	    {
		    initialiseIfNecessary();

		    //Only log if filter is set and enabled
		    if (s_filtersDictionary.ContainsKey(zFilter) && s_filtersDictionary[zFilter])
		    {
			    var logOutput = getFilterString(zFilter, zMessage);
			    if (zContext == null)
				    Debug.LogWarning(logOutput);
			    else
				    Debug.LogWarning(logOutput, zContext);
		    }
	    }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="zMessage">The message to log.</param>
        /// <param name="zFilter">The filter to use. Defaults to the General filter.</param>
        /// <param name="zContext">An optional context to pass to the Unity Logger.</param>
#if STRIP_DEBUG_LOG
	    [System.Diagnostics.Conditional("FALSE")]
#endif
	    public static void Error(object zMessage, string zFilter = k_GeneralFilter, Object zContext = null)
	    {
		    initialiseIfNecessary();

		    //Only log if filter is set and enabled
		    if (s_filtersDictionary.ContainsKey(zFilter) && s_filtersDictionary[zFilter])
		    {
			    var logOutput = getFilterString(zFilter, zMessage);
			    if (zContext == null)
				    Debug.LogError(logOutput);
			    else
				    Debug.LogError(logOutput, zContext);
		    }
	    }

        /// <summary>
        /// Log an exception message.
        /// </summary>
        /// <param name="zException">The exception to log.</param>
        /// <param name="zFilter">The filter to use. Defaults to the General filter.</param>
#if STRIP_DEBUG_LOG
	    [System.Diagnostics.Conditional("FALSE")]
#endif
	    public static void Exception(System.Exception zException, string zFilter = k_GeneralFilter)
	    {
		    initialiseIfNecessary();

		    //Only log if filter is set and enabled
		    if (s_filtersDictionary.ContainsKey(zFilter) && s_filtersDictionary[zFilter])
			    Debug.LogException(zException);
	    }

        /// <summary>
        /// Sets a filter.
        /// </summary>
        /// <param name="zFilter">The filter to set.</param>
        /// <param name="zIsEnabled">if set to <c>true</c> [enable this filter].</param>
        public static void SetFilter(string zFilter, bool zIsEnabled)
        {
            initialiseIfNecessary();

            setFilter(zFilter, zIsEnabled);
        }

        private static void initialiseIfNecessary()
        {
            //If the logger has not yet been initialised then do so now.
            if (!s_isIntitialised)
            {
                setFilter(k_GeneralFilter, true);

                s_isIntitialised = true;
            }
        }

        private static void setFilter(string zFilter, bool zIsEnabled)
        {
            if (!s_filtersDictionary.ContainsKey(zFilter))
                s_filtersDictionary.Add(zFilter, zIsEnabled);
            else
                s_filtersDictionary[zFilter] = zIsEnabled;
        }

        private static string getFilterString(string zFilter, object zMessage)
        {
            StringBuilder builder = new StringBuilder();
            return (builder.Append("[").Append(zFilter).Append("]\t").Append(zMessage.ToString())).ToString();
        }
    }
}
#endif
