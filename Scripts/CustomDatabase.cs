//----------------------------------------------------
//                    Core
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using UnityEngine;
using ClifTek.Core.FileSystem;

namespace ClifTek.Core
{
    /// <summary>
    /// This class allows for the creation of a custom database using a ScriptableObject.
    /// </summary>
    /// <remarks>
    /// This class returns a unique custom database. The database is acquired by calling Get()
    /// with the location you want the database to be stored. If the database exists it will be
    /// returned, if not a new one will be created.
    /// 
    /// Entries can be added to the database, of the form CustomDatabaseData, which are just
    /// essentially KeyValue pairs of strings. It is up to the calling code to take care of any
    /// code that might be needed to transform the data to be saved back and forth to a string
    /// representation.
    /// </remarks>
    /// <seealso cref="UnityEngine.ScriptableObject" />
    public partial class CustomDatabase : ScriptableObject
    {
        // - Private -----------------------------------------------------------
        [SerializeField] private CustomDatabaseData m_data;

        /// <summary>
        /// Gets the database file.
        /// </summary>
        /// <value>
        /// The database file.
        /// </value>
        protected File DatabaseFile { get; private set; }

        /// <summary>
        /// Creates or loads the Custom Database.
        /// </summary>
        /// <param name="zDatabaseFile">The file describing the location of the database in the project.</param>
        /// <returns>
        /// The Custom Database.
        /// </returns>
        public static CustomDatabase Get(File zDatabaseFile)
        {
            //Try to load the database from disk.
            var database = Resources.Load<CustomDatabase>(zDatabaseFile.FileName);

            //If we've loaded ok then just return.
            if (database != null)
                return database;

            //If the database does not exist yet then create now.
            database = CreateInstance<CustomDatabase>();
            database.DatabaseFile = zDatabaseFile;

#if UNITY_EDITOR
            //Actually create the asset on disk and do a save.
            Create_EditorOnly(database);
            database.Save_EditorOnly( true);
#endif

            return database;
        }

        #region Unity Messages
        private void OnEnable()
        {
            if (m_data == null)
                m_data = new CustomDatabaseData();
        }
        #endregion

        /// <summary>
        /// Gets an entry from the database.
        /// </summary>
        /// <param name="zKey">The key.</param>
        /// <returns>The entry if found, otherwise an empty string.</returns>
        public string GetEntry(string zKey)
        {
            return m_data.GetEntry(zKey);
        }
    }
}
